+++
title = "posts"
author = ["Ilmu"]
lastmod = 2018-08-23T02:30:32+00:00
draft = false
+++

## Some heading? {#some-heading}


### Setting up this blog {#setting-up-this-blog}


#### Why? {#why}

I feel I should just start blogging, it gives me the feeling I'm not working alone and who knows what it'll lead to. I am not going to put a lot of work into it, right now I am using org mode to write this file with some headings. There's probably some way I can make that into a static blog for git<something> pages. We'll explore that in the next section.


#### How? {#how}

orgmode.org has a great disambiguation of compatibility of org-mode with different static site generators. I'm using spacemacs in evil mode and tbh I have no idea how vanilla emacs w ox-hugoorks so I'm going to try to avoid having to learn too much right this moment.

It seems like the perfect solution is ox-hugo. Okay again to be honest I have looked into these static site generators before and hugo looked good. Also it'd be nice if this blog can just all be in one blog.org file since I don't really care so much about it.

Anyway they have a nice snippet for spacemacs users in the readme on the github repository and it will probably work (if you're reading this it did!)


#### Problems encountered {#problems-encountered}

Installing Hugo was possible with nix but not DNF (the fedora package manager) so the chronology of this blog is already not linear!

Also I had to look around to set some variables at the top of the org file so everything gets exported properly.


### Setting up Nix {#setting-up-nix}


#### Some backstory {#some-backstory}

I am a serial procrastinator and so it is always dangerous to start a project because it can easily change into some completely different project as I avoid working on the original intention.

Fedora is a distro that has been good at staying out of my way (I used to distro hop a lot or chase the bleeding edge until I got here). However, I've decided to invest my time in GuixSD or NixOS going forward and after having had some problems with clojurescript on Fedora I tried setting up Guix on my system but it couldn't build clojure (I ran it for a week or something before I realised that there was the option of having it use binaries but still it didn't work).

So now I'm giving Nix a try, I'm hoping it can keep my experimenting tidy without me resorting to something like Docker. Anyway, I'm past the phase where I have patience to play with the operating system when what I want is to do something on top of it.

In the future I'd prefer Guix cause I'm hardcore that way but right now I have a goal and so I need to get on with it.


#### Methodology {#methodology}

Pipe the unverified install script into bash, run \`nix-env -i clojure\` and I'm off to a promising start.

I also installed Hugo using Nix and that's where I encountered my first newb wall. The packages in my nix-env are only available in the current terminal buffer, I probably have to save this environment with some name and load it in other buffers to have access to hugo and clojure? This may be a problem when I plug my tools into things I have installed in that env.
